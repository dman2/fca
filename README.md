# Project 1

Project 1 consisted of creaying a python Flask app which would be able to backup a file. This app connected to a MYSQL database and from this the user would be able to display logs of the backup as well as three key stats, namely: number of backups, failed backups and successful backups.

# Project 2
## Objective
Automate a CI pipeline for the app built in project1 using Jenkins and Nexus.  
Containerisation is achieved using Docker and the central repository is Nexus.

## Docker Desktop
![Docker desktop containers - note that 'optimistic_elgamal' == what should be called pythonapp](https://gitlab.com/dman2/fca/-/blob/main/images/docker_1.png?ref_type=heads)  
![This needs to be input into the docker engine](https://gitlab.com/dman2/fca/-/blob/main/images/docker_2.png?ref_type=heads)  
![Continuation of line above](https://gitlab.com/dman2/fca/-/blob/main/images/docker_3.png?ref_type=heads)

## Docker commands to be inputted into windows cmd
![Screenshot 1](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd001.png?ref_type=heads)  
![Screenshot 2](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd002.png?ref_type=heads)  
![Screenshot 3](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd003.png?ref_type=heads)  
![Screenshot 4](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd004.png?ref_type=heads)  
![Screenshot 5](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd1.png?ref_type=heads)  
![Screenshot 6](https://gitlab.com/dman2/fca/-/blob/main/images/docker_cmd2.png?ref_type=heads)

## Nexus
Nexus credentials:  
username - admin  
password - password123  
*note*  *initial password comes from command line when running the run.sh file*  
![Create a new blob store, 'docker', in Nexus](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_1.png?ref_type=heads)  
![What's contained within the new blob](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_1.png?ref_type)  
![Create new pipeline repository, 'docker-hub2'](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_3.png?ref_type=heads)  
![Information within the new repository](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_4.png?ref_type=heads)  
![Information within the new repository](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_5.png?ref_type=heads)  
![Information within the new repository](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_6.png?ref_type=heads)  
![Ensure active realms look like this](https://gitlab.com/dman2/fca/-/blob/main/images/nexus_7.png?ref_type=heads)  

## Jenkins
Jenkins credentials:   
username - duncan  
password - root (or password lol) 


